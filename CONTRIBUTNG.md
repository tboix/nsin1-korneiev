# Code structure
`Provisional version:`

    project-name/
        demo/
        docs/
        support/
            img/
        CHANGELOG.md
        CONTRIBUTING.md
        Dockerfile
        LICENSE
        README.md


## README.md
The README.md can contain the following information, if no dedicated file or support has been created for each case:

- The description of the project.
- Disclaimers.
- Installation, compiling, distributing or deployment support information for production and development environments.
- Interfaces user/developer guides.
- Author/Owner and support contact information.

## demo/
The demo folder should contain any piece of code created to test, manipulate or demonstrate any usage of the project.

> The code of the demo folder should not be distribted in any case.

## docs/
This folder should contain any other type of documentation, more detailed than the documentation provided in the root of the project, regarding specific usages or for example auto generated library documentation.

## support/
This folder should contain templates or any other material useful for the development process.

> The code of the support folder should not be distribted in any case.

## CHANGELOG.md
This file must contain a human version record of any change distributed as a version of the project.

## Dockerfile
`To be defined.`

## LICENSE
`To be defined`

# Distribution
`To be defined.`

## Version numbers
This project uses [Sequence-based identifiers](https://en.wikipedia.org/wiki/Software_versioning) as software versioning. In subsequent releases, the **major** number is increased when there are significant jumps in functionality such as changing the framework which could cause incompatibility with interfacing systems, the **minor** number is incremented when only minor features or significant fixes have been added, and the **revision** number is incremented when minor bugs are fixed. `The format of each tag is v[major].[minor].[release]`. e.g:

    v1.3.5

The major is 1, the minor is 3 and release is 5. Each release tag must be created from **master** branch and must match this format.

# Continuous code quality
## Violations
`To be defined.`

## Unitary testing
`To be defined.`

# SCM policy
## Branching
This project workflow is based in [Gitflow workflow](http://nvie.com/posts/a-successful-git-branching-model/). All branches must be created and merged following the **Gitflow** conventions except the release branch that will not be used for this project because there are not differences between release and production environment.

There are two main branches **development** and **master**. `Master branch must not be modified never except when changes are merged back into master from development branch or a hotfix branch`. The development branch should be used to integrate all the changes from feature branches before to merge back into master branch. Each time changes are merged back into master, this is a new production release by definition and should be tagged.

### Hotfix branch
A hotfix branch should be created when a bug is detected and must be fixed at master branch. Hotfix branches are created from the master branch. When finished, the bugfix needs to be merged back into master, but also needs to be merged back into develop, in order to safeguard that the bugfix is included in the next release as well.

![gitflow hotfix image](support/img/git-workflow-hotfix-reintegration.png)

### Development branch
Development branch should be used to implement minor modifications over existing code or to integrate feature branches before merge back into master branch.

![gitflow development image](support/img/git-workflow-devel.png)

### Feature branch
Features branches should be created when new significant amount of code must be implemented or when performing significant modifications on existing code.

![gitflow feature image](support/img/git-workflow-feature.png)

## Revision process
The revision process are made using the *pull request* mechanism provided by Bitbucket. Any merge to the master branch should be made via a *pull request* and requires at least the approval of 1 authorized reviewer. A list of authorized reviewers should be provided at the top of **README.md** file in the **Reviewers** section.
