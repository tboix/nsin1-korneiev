# Introduction
NSIN1-KORNEIEV project contains the front-end server implementation or configuration options, the [Docker](https://www.docker.com/) helper files used to build an image and any other source code file required by the Dockerfile to build the image.

# Author
Pull requests authorized reviewers:

- Antoni Boix antoni.boix@ficosa.com
- Joan Garcia joan.garcia@ficosa.com

# Build
To be defined.

# Deployment: Development
To be defined.
